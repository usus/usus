//
// usus.js
//
// (c) 2014. Sanjeev Premi (spremi@ymail.com)
//
// This software is available under terms of The MIT License.
// (http://opensource.org/licenses/MIT)
//

/**
 * Version of this application.
 */
var USUS_VERSION = "0.10";

/**
 * Name of file containing list of builds to be considered for viewing.
 */
var TXT_BUILD_LIST = "./build-list.txt";

/**
 * Name of file containing list of packages associated with a build.
 */
var TXT_PKG_LIST = "pkg-list.txt";

/**
 * Name of file containing list of test suites executed on the built packages.
 */
var TXT_TEST_LIST = "tst-list.txt";

/**
 * Pattern used to identify the containers for build log corresponding to
 * the selected build.
 *
 * Multiple packages may be considered in a build. This pattern will be
 * used to derive exact name of container for a specific package.
 */
var TXT_PKG_BLDLOG = "bldpkg";

/**
 * Pattern used to identify the containers for execution log corresponding to
 * the test-suites deployed for the selected build.
 *
 * Multiple test-suites may be deployed for a build. This pattern will be
 * used to derive exact name of container for a specific test-suite.
 */
var TXT_TST_EXELOG = "bldtst";

/**
 * Regular expression to parse each line in TXT_BUILD_LIST.
 */
var REGEX_BUILD_LIST = /\s*(.*)\|\s*(.*)$/;

/**
 * Regular expression to parse each line in TXT_PKG_BLDLOG*.
 */
var REGEX_BUILD_LOG = /\[(.*)\]\s*(.*)$/;

/**
 * Regular expression to parse each line in TXT_TST_EXELOG*.
 */
var REGEX_TEST_LOG = /\[(.*)\]\s*(.*?)(\s*\:\s*(.*))?$/;

/**
 * Maximum height (in px) of the container for build list.
 */
var HEIGHT_BUILDLIST = 300;

/**
 * Maximum height (in px) of the container for build list.
 */
var HEIGHT_THEMELIST = 200;

/**
 * Maximum height (in px) of the container for log messages.
 */
var HEIGHT_LOG = 250;

/**
 * Severity of message: Normal information
 */
var LOG_INFO = 0;

/**
 * Severity of message: Debug
 */
var LOG_DEBUG = 1;

/**
 * Severity of message: Warning
 */
var LOG_WARN = 2;

/**
 * Severity of message: Error
 */
var LOG_ERROR = 3;

/**
 * Prefix for containers with "raw" data.
 */
var PREFIX_RAW = 'r-';

/**
 * Prefix added to build log of each package.
 */
var PREFIX_BLDLOG = 'bld-';

/**
 * Suffix (including file extension) added to build log of each package.
 */
var SUFFIX_BLDLOG = '.txt';

/**
 * Prefix added to execution log of each test-suite.
 */
var PREFIX_EXELOG = 'tst-';

/**
 * Prefix added to each selectable theme.
 */
var PREFIX_THEME = 't-';

/**
 * Suffix (including file extension) added to execution log of each test=suite.
 */
var SUFFIX_EXELOG = '.txt';

/**
 * Suffix for the summary container.
 */
var SUFFIX_SUMMARY = '-s';

/**
 * Token - Name of package
 */
var TKN_PKG_NAME = 'PKG';

/**
 * Token - Version of package
 */
var TKN_PKG_VERS = 'VER';

/**
 * Token - Build status
 */
var TKN_PKG_STAT = 'STATUS';

/**
 * Token - Name of test suite
 */
var TKN_TST_SUITE = 'TSUITE';

/**
 * Token - Name of test case
 */
var TKN_TST_CASE = 'TCASE';

/**
 * Token - Name of test category
 */
var TKN_TST_CAT = 'TCAT';

/**
 * Token - Test result
 */
var TKN_TST_RESULT = 'TRESULT';

/**
 * Animation speed (in msec) for fading in the splash background.
 */
var SPLASH_BG_FADEIN = 1000;

/**
 * Animation speed (in msec) for fading out the splash background.
 */
var SPLASH_BG_FADEOUT = 500;

/**
 * Animation speed (in msec) for fading in the splash foreground.
 */
var SPLASH_FG_FADEIN = 1000;

/**
 * Animation speed (in msec) for fading out the splash foreground.
 */
var SPLASH_FG_FADEOUT = 500;

/**
 * Duration (in msec) to show the splash foreground.
 */
var SPLASH_WAIT = 1000;

/**
 * Animation speed (in msec) for showing an element.
 */
var ANIM_SHOW = 500;

/**
 * Animation speed (in msec) for hiding an element.
 */
var ANIM_HIDE = 250;


// =============================================================================

/**
 * Number of builds to show.
 */
var NumBuilds = 0;

/**
 * Flag: Is container '#bldlist' visible?
 */
var BldlistVisible = false;

/**
 * Flag: Is container '#log' visible?
 */
var LogVisible = false;

/**
 * Flag: Is container '#themes' visible?
 */
var ThemesVisible = false;

/**
 * Array of packages built
 */
var ArrPkgsBuilt = [];

/**
 * Array of test suites executed
 */
var ArrTstSuites = [];

/**
 * Array of available themes
 */
var ArrThemes = ['default', 'royale'];


// =============================================================================

/**
 * Inserts version string into the page footer.
 */
function showVersion()
{
	var s = '(v' + USUS_VERSION + ' using JQuery v' + $.fn.jquery + ')';

	$('#version').html(s);
}

/**
 * Show splash screen.
 *
 * @param fgid	ID of the foreground element
 * @param bgid	ID of the background element
 *
 * Basic properties of the splash screen are controlled by the CSS.
 * This function is responsible for animation.
 */
function showSplash(fgid, bgid)
{
	var fg = '#' + fgid;
	var bg = '#' + bgid;

	//
	// Set attributes of the background element.
	//
	$(bg).css('display', 'none');

	//
	// Set attributes of the foreground element.
	// Specifically, position at center of the window.
	//
	$(fg).css('display', 'none');
	$(fg).css("top", Math.round(($(window).height() -
	$(fg).height()) / 2)  + "px");
	$(fg).css("left", Math.round(($(window).width() -
	$(fg).width()) / 2)  + "px");

	$(fg + ' h3').html('(Version ' + USUS_VERSION + ')');

	//
	// Handle 'click'
	//
	$(fg).on('click', function () {
		$(bg).stop(true, true).hide();
		$(fg).stop(true, true).hide();
	});

	//
	// Splash animation
	//
	$(bg).fadeTo(SPLASH_BG_FADEIN, 0.95);

	$(fg).fadeIn(SPLASH_FG_FADEIN)
		.delay(SPLASH_WAIT)
		.fadeOut(SPLASH_FG_FADEOUT, function () {
			$(bg).fadeOut(SPLASH_BG_FADEOUT);
			}
	);
}

/**
 * Initialize log container.
 */
function initLog()
{
	$('#log').html('<h3>Log Messages</h3>');
}

/**
 * Add log message with associated severity.
 *
 * @param  svr	Severity
 * @param  msg	Message text
 */
function addLog(svr, msg)
{
	var s = '';
	var c = '';

	switch (svr) {
	case LOG_ERROR:
		c = 'error';
		break;

	case LOG_WARN:
		c = 'warn';
		break;

	case LOG_DEBUG:
		c = 'debug';
		break;

	default:
		c = 'info';
		break;
	}

	s = '<p class="' + c + '">' + msg + '</p>';

	$('#log').append(s);

	return false;
}

/**
 * Add themes menu
 */
function addThemes()
{
	var s = '';

	for (var i = 0; i < ArrThemes.length; i++) {
		var tid = PREFIX_THEME + ArrThemes[i];

		s += '<a id="' + tid + '" href="#">' + ArrThemes[i] + '</a>';
	}

	$('#themes').html(s);

	//
	// Bind click event to the theme IDs.
	// Dynamic binding is possible only after the elements are inserted
	// into the DOM.
	//
	for (var i = 0; i < ArrThemes.length; i++) {
		var e = '#' + PREFIX_THEME + ArrThemes[i];

		$(e).bind( "click", changeTheme);
	}
}

/**
 * Toggle visibility of container for list of available builds
 */
function toggleBuildList()
{
	var h = HEIGHT_BUILDLIST;

	if (BldlistVisible) {
		$('#menu-bg').fadeOut(ANIM_HIDE);

		//
		// Animate container out of view
		//
		$('#bldlist')
			.stop(true, true)
			.css('overflow-y', 'hidden')
			.animate({ top: "+=" + h, height: "-=" + h}, ANIM_HIDE, function () {
				$(this).hide();
				$('#m-builds').fadeTo(0, 1.0);
		});

		BldlistVisible = false;
	} else {
		var t;
		var l;

		//
		// Recalculate location of the container
		//
		t = $('#menu').offset().top;
		l = $('#m-builds').offset().left;

		$('#bldlist').css({top: t, left: l});

		//
		// Animate container into view
		//
		$('#menu-bg').fadeTo(ANIM_SHOW, 0.8);

		$('#bldlist')
			.stop(true, true)
			.css('overflow-y', 'auto')
			.show()
			.animate({ top: "-=" + h, height: "+=" + h}, ANIM_SHOW, function () {
				$('#m-builds').fadeTo(0, 0.5);
		});

		BldlistVisible = true;
	}

	return false;
}

/**
 * Toggle visibility of container for log messages
 */
function toggleLog()
{
	var h = HEIGHT_LOG;

	if (LogVisible) {
		//
		// Animate container out of view
		//
		$('#log')
			.stop(true, true)
			.css('overflow-y', 'hidden')
			.animate({ top: "+=" + h, height: "-=" + h}, ANIM_HIDE, function () {
				$(this).hide();
				$('#m-show-log').html("Show log").fadeTo(0, 1.0);
		});

		LogVisible = false;
	} else {
		var t;

		//
		// Recalculate location of the container
		//
		t = $('#menu').offset().top;

		$('#log').css({top: t});

		//
		// Animate container into view
		//
		$('#log')
			.stop(true, true)
			.css('overflow-y', 'auto')
			.show()
			.animate({ top: "-=" + h, height: "+=" + h}, ANIM_SHOW, function () {
				$('#m-show-log').html("Hide log").fadeTo(0, 0.5);
		});

		LogVisible = true;
	}

	return false;
}

/**
 * Toggle visibility of themes menu
 */
function toggleThemes()
{
	var h = HEIGHT_THEMELIST;

	if (ThemesVisible) {
		//
		// Animate container out of view
		//
		$('#themes')
			.stop(true, true)
			.css('overflow-y', 'hidden')
			.animate({ top: "+=" + h, height: "-=" + h}, ANIM_HIDE, function () {
				$(this).hide();
				$('#m-themes').fadeTo(0, 1.0);
		});

		ThemesVisible = false;
	} else {
		var t;
		var l;

		//
		// Recalculate location of the container
		//
		t = $('#menu').offset().top;
		l = $('#m-themes').offset().left;

		$('#themes').css({top: t, left: l});

		//
		// Animate container into view
		//
		$('#themes')
			.stop(true, true)
			.css('overflow-y', 'auto')
			.show()
			.animate({ top: "-=" + h, height: "+=" + h}, ANIM_SHOW, function () {
				$('#m-themes').fadeTo(0, 0.5);
			});

		ThemesVisible = true;
	}

	return false;
}

/**
 * Change current theme to match selection.
 *
 * To avoid abrupt change - leading to flicker, transition the theme through
 * a fade in-and-out of a background element.
 */
function changeTheme()
{
	var theme;

	toggleThemes();

	theme = $(this).attr("id").substring(PREFIX_THEME.length);

	$('#themes-bg').fadeIn(ANIM_SHOW,
			       function () {
					document.getElementById('theme')
						.href = 'css/themes/' + theme + '.css';

					$('#themes-bg').delay(250).fadeOut(ANIM_HIDE);
				}
			);
}

/**
 * Populate the menubar.
 */
function addMenu()
{
	var s;

	//
	// Placeholder for currently selected build
	//
	s = '&laquo;&nbsp;No Selection&nbsp;&raquo;'

	$('<div id="c-build">' + s + '</div>').appendTo('#menu');

	//
	// Menuitem: Selecting a build
	//
	s = 'Available builds'

	$('<div id="m-builds" class="m-item">' + s + '</div>').appendTo('#menu');

	//
	// Menuitem: Show log
	//
	s = 'Show log'

	$('<div id="m-show-log" class="m-item">' + s + '</div>').appendTo('#menu');

	//
	// Menuitem: Clear log
	//
	s = 'Clear log'

	$('<div id="m-clear-log" class="m-item">' + s + '</div>').appendTo('#menu');

	//
	// Menuitem: Change theme
	//
	s = 'Select theme'

	$('<div id="m-themes" class="m-item">' + s + '</div>').appendTo('#menu');

	addThemes();

	//
	// Bind events
	//
	$('#m-builds').bind( "click", toggleBuildList);
	$('#m-show-log').bind( "click", toggleLog);
	$('#m-clear-log').bind( "click", initLog);
	$('#m-themes').bind( "click", toggleThemes);
}

/**
 * Add placeholder for raw data received in response to AJAX requests.
 * The top-level container is hidden via CSS. So, it is never rendered
 * in the browser.
 */
function addRawVault()
{
	$('<div id="raw" />').css("display", "none").appendTo('body');

	$('<div id="r-bldlist" />').appendTo('#raw');
	$('<div id="r-pkglist" />').appendTo('#raw');
	$('<div id="r-tstlist" />').appendTo('#raw');
}

/**
 * Initialize build summary.
 *
 * Contents must be initialized once. presence of <table> tag is used as invariant
 * for the purpose.
 */
function initBuildSummary()
{
	var sid = '#' + TXT_PKG_BLDLOG + SUFFIX_SUMMARY;

	if ($(sid + ':has(table)').length == 0) {
		$(sid).html('<table>' +
			'<thead>' +
			'<tr>' +
			'<th>Package</th>' +
			'<th>Version</th>' +
			'<th>Status</th>' +
			'</tr>' +
			'</thead>' +
			'<tbody>' +
			'</tbody>' +
			'</table>');
	}
}

/**
 * Initialize test summary.
 *
 * Contents must be initialized once. presence of <table> tag is used as invariant
 * for the purpose.
 */
function initTestSummary()
{
	var sid = '#' + TXT_TST_EXELOG + SUFFIX_SUMMARY;

	if ($(sid + ':has(table)').length == 0) {
		$(sid).html('<table>' +
		'<thead>' +
		'<tr>' +
		'<th>Test Suite</th>' +
		'<th>Test Case</th>' +
		'<th>Test Category</th>' +
		'<th>Result</th>' +
		'</tr>' +
		'</thead>' +
		'<tbody>' +
		'</tbody>' +
		'</table>');
	}
}

/**
 * Parse build log
 */
function parseBuildLog(response, status, xhr)
{
	var lines = [];
	var tid;	// Id of target container.
	var sid;	// Id of 'summary' container.
	var tag;
	var str;

	//
	// Derive container "id" from the "id" of this container
	//
	tid = $(this).attr("id").substring(PREFIX_RAW.length);
	tag = '#' + tid;

	if (status == 'success') {
		addLog(LOG_DEBUG, 'Received build log for ' + tid);

		str = '<p class="log">' + $(this).text() + '</p>';
		$(tag).empty().html(str);

		sid = '#' + TXT_PKG_BLDLOG + SUFFIX_SUMMARY;

		initBuildSummary();

		//
		// Convert text blob into an array across the newline character.
		//
		lines = $(this).text().trim().split("\n");

		//
		// Parse for necessary information
		//
		var pkgName = '';
		var pkgVers = '';
		var pkgStat = '';
		var clsStat = '';

		for (var i = 0; i < lines.length; i++) {
			var line = lines[i].trim();

			var res = line.match(REGEX_BUILD_LOG);
			if (res) {
				switch (res[1]) {
				case TKN_PKG_NAME:
					pkgName = res[2];
					break;
				case TKN_PKG_VERS:
					pkgVers = res[2];
					break;
				case TKN_PKG_STAT:
					pkgStat = res[2];
					break;
				default:
					break;
				}
			}
		}

		clsStat = pkgStat.charAt(0).toLowerCase();  ;

		$(sid + ' tbody').append ('<tr>' +
						'<td>' + pkgName + '</td>' +
						'<td>' + pkgVers + '</td>' +
						'<td class="' + clsStat + '">' + pkgStat + '</td>' +
						'</tr>');

		$(this).empty();
	} else {
		s = 'Couldn\'t get build log for ' + tid;

		addLog(LOG_ERROR, s);
		$(tag).html('<p id="err">' + s + '</p>');
	}
}

/**
 * Parse test executuin log
 */
function parseTestLog(response, status, xhr)
{
	var tid;	// Id of target container.
	var tag;
	var str;

	//
	// Derive container "id" from the "id" of this container
	//
	tid = $(this).attr("id").substring(PREFIX_RAW.length);
	tag = '#' + tid;

	if (status == 'success') {
		addLog(LOG_DEBUG, 'Received test execution log for ' + tid);

		str = '<p class="log">' + $(this).text() + '</p>';
		$(tag).empty().html(str);
		sid = '#' + TXT_TST_EXELOG + SUFFIX_SUMMARY;

		initTestSummary();

		//
		// Convert text blob into an array across the newline character.
		//
		lines = $(this).text().trim().split("\n");

		//
		// Parse for necessary information
		//
		var tstSuite  = '';
		var tstCase   = '';
		var tstCat    = '';
		var tstResult = '';

		for (var i = 0; i < lines.length; i++) {
			var line = lines[i].trim();

			var res = line.match(REGEX_TEST_LOG);
			if (res) {
				switch (res[1]) {
				case TKN_TST_SUITE:
					tstSuite = res[2];
					break;
				case TKN_TST_CASE:
					tstCase = res[2];
					break;
				case TKN_TST_CAT:
					tstCat = res[2];
					break;
				case TKN_TST_RESULT:
					if (tstCase == res[2]) {
						tstResult = res[4];
					} else {
						tstResult = 'Unknown';
					}

					var r = tstResult.charAt(0).toLowerCase();  ;

					$(sid + ' tbody').append ('<tr>' +
								'<td>' + tstSuite + '</td>' +
								'<td>' + tstCase + '</td>' +
								'<td>' + tstCat + '</td>' +
								'<td class="' + r + '">' + tstResult + '</td>' +
								'</tr>');
					break;
				default:
					break;
				}
			}
		}

		$(this).empty();
	} else {
		s = 'Couldn\'t get test execution log for ' + tid;

		addLog(LOG_ERROR, s);
		$(tag).html('<p id="err">' + s + '</p>');
	}
}

/**
 * Parse contents of file containing list of packages built.
 */
function parsePkgList(response, status, xhr)
{
	var lines = [];
	var s = '';
	var x = '';
	var y = '';

	if (status == 'success') {
		addLog(LOG_DEBUG, 'Received list of packages');

		//
		// Convert text blob into an array across the newline character.
		//
		lines = $("#r-pkglist").text().trim().split("\n");

		//
		// Empty the array before re-use.
		//
		while (ArrPkgsBuilt.length) {
			ArrPkgsBuilt.pop();
		}

		//
		// Extract name of packages
		//
		for (var i = 0; i < lines.length; i++) {

			var line = lines[i].trim();

			if (line != "") {
				ArrPkgsBuilt[i] = line;
			}
		}

		//
		// Show build log for packages as TAB
		//
		if (ArrPkgsBuilt.length > 0) {
			var tag = '#' + TXT_PKG_BLDLOG;
			var tid;

			$('#bldinfo').empty().html('<div id="' + TXT_PKG_BLDLOG + '"></div>');

			$(tag).css({display: 'none'});

			//
			// Add TAB for summary information
			//
			tid = TXT_PKG_BLDLOG + SUFFIX_SUMMARY;

			x += '<li><a href="#' + tid + '">Summary</a></li>'
			y += '<div id="' + tid + '">' +
				'<p>Wait for analysis...</p>' +
				'</div>';

			for (var i = 0; i < ArrPkgsBuilt.length; i++) {
				var tid = TXT_PKG_BLDLOG + '-' + i;

				x += '<li><a href="#' + tid + '">' + ArrPkgsBuilt[i] + '</a></li>'
				y += '<div id="' + tid + '"><p>Fetching...</p></div>'
			}

			s  = '<ul>';
			s += x;
			s +='</ul>';
			s += y;

			$(tag).html(s);
			$(tag).tabs();
			$(tag).slideDown(ANIM_SHOW);

			//
			// Request contents of each log file for selected build
			//
			var bld = $('#c-build').html();

			for (var i = 0; i < ArrPkgsBuilt.length; i++) {

				var rfile = './' + bld + '/build/' +
						PREFIX_BLDLOG +
						ArrPkgsBuilt[i] +
						SUFFIX_BLDLOG;

				var rid   = PREFIX_RAW + TXT_PKG_BLDLOG + '-' + i;
				var rtag  = '#' + rid;

				//
				// Append container for raw log.
				//
				$('<div id="' + rid  + '" />').appendTo('#raw');

				//
				// Request build log
				//
				addLog(LOG_DEBUG, 'Requesting file ' + rfile);
				$(rtag).load(rfile, parseBuildLog);
			}

		} else {
			s = 'No Packages were built.';

			addLog(LOG_WARN, s);
			$('#bldinfo').empty().html('<p class="inf">' + s + '</p>');
		}
	} else   {
		s = 'Couldn\'t get list of packages built';

		addLog(LOG_ERROR, s);
		$('#bldinfo').empty().html('<p class="err">' + s + '</p>');
	}

	//
	// Clear #r-pkglist
	//
	$("#r-pkglist").text('');

	return false;
}

/**
 * Parse contents of file containing list of testsuites executed.
 */
function parseTstList(response, status, xhr)
{
	var lines = [];
	var s = '';
	var x = '';
	var y = '';

	var ntsts = 0;

	if (status == 'success') {
		addLog(LOG_DEBUG, 'Received list of test suites');

		//
		// Convert text blob into an array across the newline character.
		//
		lines = $("#r-tstlist").text().trim().split("\n");

		//
		// Empty the array before re-use.
		//
		while (ArrTstSuites.length) {
			ArrTstSuites.pop();
		}

		//
		// Extract name of test suites
		//
		for (var i = 0; i < lines.length; i++) {

			var line = lines[i].trim();

			if (line != "") {
				ArrTstSuites[i] = line;
			}
		}

		//
		// Show test logs information as TAB
		//
		if (ArrTstSuites.length > 0) {
			var tag = '#' + TXT_TST_EXELOG;
			var tid;

			$('#tstinfo').empty().html('<div id="' + TXT_TST_EXELOG + '"></div>');

			$(tag).css({display: 'none'});

			//
			// Add TAB for summary information
			//
			tid = TXT_TST_EXELOG + SUFFIX_SUMMARY;

			x += '<li><a href="#' + tid + '">Summary</a></li>'
			y += '<div id="' + tid + '">' +
				'<p>Wait for analysis...</p>' +
				'</div>';

			for (var i = 0; i < ArrTstSuites.length; i++) {
				tid = TXT_TST_EXELOG + '-' + i;

				x += '<li><a href="#' + tid + '">' + ArrTstSuites[i] + '</a></li>'

				y += '<div id="' + tid + '"><p>Fetching...</p></div>'
			}

			s  = '<ul>';
			s += x;
			s +='</ul>';
			s += y;

			$(tag).html(s);
			$(tag).tabs();
			$(tag).slideDown(ANIM_SHOW);

			//
			// Request contents of each log file
			//
			var bld = $('#c-build').html();

			for (var i = 0; i < ArrTstSuites.length; i++) {

				var rfile = './' + bld + '/test/' +
						PREFIX_EXELOG +
						ArrTstSuites[i] +
						SUFFIX_EXELOG;

				var rid   = PREFIX_RAW + TXT_TST_EXELOG + '-' + i;
				var rtag  = '#' + rid;

				//
				// Append container for raw log.
				//
				$('<div id="' + rid  + '" />').appendTo('#raw');

				//
				// Request build log
				//
				addLog(LOG_DEBUG, 'Requesting file ' + rfile);
				$(rtag).load(rfile, parseTestLog);
			}
		} else {
			s = 'No test-suites were executed.';

			addLog(LOG_WARN, s);
			$('#tstinfo').empty().html('<p class="inf">' + s + '</p>');
		}
	} else   {
		s = 'Couldn\'t get list of test-suites.';

		addLog(LOG_ERROR, s);
		$('#tstinfo').empty().html('<p class="err">' + s + '</p>');
	}

	//
	// Clear #r-tstlist
	//
	$("#r-tstlist").text('');

	return false;
}

/**
 * Extract build ID from the element that was clicked.
 * Use this ID to fetch details information related to the build.
 */
function getBuildInfo()
{
	var bld;
	var blog;
	var tlog;

	//
	// Extract build id from the clicked element
	//
	bld = $(this).text();

	addLog(LOG_INFO, 'Selected build ' + bld);

	//
	// Set name of current build and the list
	//
	$('#c-build').html(bld);
	toggleBuildList();

	//
	// Derive name of files containing list of packages built and list
	// of test cases associated with the build.
	//
	blog = './' + bld + '/' + TXT_PKG_LIST;
	tlog = './' + bld + '/'  + TXT_TEST_LIST;

	//
	// Load raw build information.
	//
	addLog(LOG_DEBUG, 'Requesting file ' + blog);

	$("#r-pkglist").load(blog, parsePkgList);

	//
	// Load raw build information.
	//
	addLog(LOG_DEBUG, 'Requesting file ' + tlog);

	$("#r-tstlist").load(tlog, parseTstList);
}

/**
 * Parse the list of available builds and update '#bldlist"
 */
function parseBuildList(response, status, xhr)
{
	var lines = [];
	var s = '';
	var x = '';

	if (status == 'success') {
		addLog(LOG_DEBUG, 'Received file ' + TXT_BUILD_LIST);

		//
		// Convert text blob into an array across the newline character.
		//
		lines = $("#r-bldlist").text().trim().split("\n");

		//
		// Parse each line and collate the build information.
		//
		for (var i = 0; i < lines.length; i++) {

			var line = lines[i].trim();

			if (line.match(REGEX_BUILD_LIST)) {
				var cols = line.split(/\s*\|\s*/);
				var id = 'bld' + i;

				x += '<tr><td><a id="' + id + '" href="#">' + cols[0] + '</a></td>'
				x += '<td>' + cols[1] + '</td></tr>';

				NumBuilds++;
			}
		}

		//
		// Format build information, if present, as a table
		//
		if (NumBuilds > 0) {
			s  = '<table>';
			s += '<tr><th>Build ID</th><th>Start Time</th></tr>';
			s += x;
			s +='</table>';
		} else {
			s = '<p class="inf">No builds available.</p>';
		}
	} else   {
		addLog(LOG_ERROR, 'Couldn\'t get file ' + TXT_BUILD_LIST);

		s = '<p class="err">Couldn\'t load build information.</p>';
	}

	//
	// Update the #bldlist and clear #r-bldlist
	//
	$("#bldlist").html(s);
	$("#r-bldlist").text('');

	//
	// Bind click event to the build IDs.
	// Dynamic binding is possible only after the elements are inserted
	// into the DOM.
	//
	for (var i = 0; i < NumBuilds; i++) {
		var e = '#bld' + i;

		$(e).on("click", getBuildInfo);
	}

	//
	//  Update contents of #bldinfo and #tstinfo
	//
	if (NumBuilds > 0) {
		s = '<p class="inf">Select a build to display information.</p>';
	} else   {
		s = '<p class="inf">No builds available.</p>';
	}

	$("#bldinfo").html(s);
	$("#tstinfo").html(s)
}

/**
 * Get list of available builds, parse it and update '#bldlist".
 */
function getBuildList()
{
	//
	// Load raw build information.
	//
	addLog(LOG_DEBUG, 'Requesting file ' + TXT_BUILD_LIST);

	$("#r-bldlist").load(TXT_BUILD_LIST, parseBuildList);
}


// =============================================================================
/**
 * Sequence of actions to be performed when DOM is fully loaded.
 *
 */
$(document).ready(function() {
	//
	// Show splash screen
	//
	showSplash('splash', 'splash-bg');

	//
	// Show version information
	//
	showVersion();

	//
	// Initialize log container
	//
	initLog();

	//
	// Populate the menubar
	//
	addMenu();

	//
	// Add hidden vault for raw data received via AJAX requests
	//
	addRawVault();

	//
	// Get list of available builds
	//
	if (NumBuilds == 0) {
		getBuildList();
	}
});
